"use strict";

const currentTakerFee = 0.0015; //0.15% used to be zero
const currentMakerFee = 0.0025; //0.25% stayed the same
const profit = 0.1; //10% profit

exports.calcTakerFee = (size, price) => {
    let fee = size * price * currentTakerFee;

    return fee;
}

exports.calcMakerFee = (size, price) => {
    let fee = size * price * currentMakerFee;

    return fee;
}

exports.profitPrice = (price, size) => {
    let buyFee = exports.calcTakerFee(size, price);
    let newSellPrice = parseFloat(price) * (1.0 + profit); 
    let sellFee = exports.calcTakerFee(newSellPrice, size);

    let totalSellPrice = parseFloat(newSellPrice  + buyFee + sellFee);
    return totalSellPrice;
}