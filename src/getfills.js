"use strict";

let coinbase = require('./coinbase.js');
let fee = require('./fee.js');

let authenticatedClient = coinbase.authenticatedClient;

const calcProfit = (error, response, data) => {
    if (error) {
        console.log(error)
        return
    }
    for (var item of data) {
       if (item.side == 'buy' && item.settled) {
        let profitPrice = fee.profitPrice(item.price, item.size); 
        let buyFee = fee.calcTakerFee(item.price, item.size)
        let cost = (parseFloat(item.size * item.price) + parseFloat(item.fee)).toFixed(6);
        let output = item.trade_id + ' ' + item.product_id + ' ' + item.side + ' ' + item.size + ' ' + item.price + ' '+ item.fee + ' == ' + buyFee.toFixed(6) + ' ' + cost + '| ' + profitPrice.toFixed(6); 
        console.log(output);
       }  
    }

    //console.log(data)
    //filled = data[0]
    //bot.sendMessage(telegram.chatid, `${filled.created_at} :: _*${filled.side}* ${filled.product_id}_ *Filled* for ${filled.size} price ${filled.price} euro. fee ${filled.fee}.`,{'parse_mode': 'Markdown'});
}

const params = {
    product_id: 'XLM-EUR',
    before: 311654
  };

  authenticatedClient.getFills(params, calcProfit);
