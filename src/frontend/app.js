import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(Vuetify)

new Vue({
    el: '#app',
    template: '<App/>',
    components: { App }
})