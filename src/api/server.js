var express = require('express');
var app = express();
let ws = require('./websocket');
let cors = require('cors');
let bodyParser = require('body-parser');

ws.start();
console.log('Socket to coinbase started .....');

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
}
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

app.use(function(req, res, next){
    const origin = req.get('origin');
    /*
    res.header('Access-Control-Allow-Origin','*');

    res.header('Access-Control-Allow-Methods','GET,POST,PUT,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    */
    next();
});

require('./routes')(app);
app.listen(3001);
console.log('Listening on port 3001...');
