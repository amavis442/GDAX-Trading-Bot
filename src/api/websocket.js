let coinbase = require(__dirname + '/../coinbase.js');
let CoinbasePro = coinbase.CoinbasePro;
let cache = {};


const init_ws_stream = () => {
    websocket = new CoinbasePro.WebsocketClient(
        ['XRP-EUR', 'XLM-EUR', 'BTC-EUR', 'ETC-EUR', 'ETH-EUR', 'LTC-EUR', 'BAT-ETH', 'ZRX-EUR'], 
        coinbase.WS_URI, {
            key: coinbase.KEY,
            secret: coinbase.SECRET,
            passphrase: coinbase.PASSPHRASE,
        }, {
            heartbeat: true,
            channels: ['heartbeat', 'ticker']
        })

    websocket.on('message', (data) => {
        switch (data.type) {
            case "heartbeat":
                return
            default:
                process_ws_message(data)
                break
        }
    })

    websocket.on('error', (error) => {
        console.log(error)
    })

    websocket.on('close', (data) => {
        ws_reconnect(websocket, data)
    })
}

const ws_reconnect = (ws, data) => {
    console.log(`CoinbasePro websocket disconnected with data: ${data}`)
    // try to re-connect the first time...
    ws.connect()
    let count = 1
    // attempt to re-connect every 30 seconds.
    // TODO: maybe use an exponential backoff instead
    const interval = setInterval(() => {
        if (!ws.socket) {
            console.log(`Reconnecting to CoinbasePro (attempt ${count++})`)
            //count++
            ws.connect()
        } else {
            console.log('CoinbasePro reconnected')
            clearInterval(interval)
        }
    }, 10000)
}

const process_ws_message = (data) => {

    switch (data.type) {
        case 'ticker': {
            cache[data.product_id] = data.price;
        } break
    }
}

exports.start = function () {
    init_ws_stream();
}
exports.cache = cache;