module.exports = function (app) {
    let wallets = require('./controllers/wallets').wallets;
    let products = require('./controllers/products').products;

    app.get('/balance', wallets.getBalanceValue);
    app.get('/getbalance/:symbol', wallets.getBalance);
    app.get('/tickers', products.getTickers);

    app.get('/getticker/:symbol', products.getTicker);
}