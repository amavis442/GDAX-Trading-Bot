"use strict";
require('colors');
let coinbase = require(__dirname + '/../../coinbase.js');
let authenticatedClient = coinbase.authenticatedClient;
let cache = require(__dirname + '/../websocket.js').cache;
let balances = {};
let eurPair = {
    'BTC': 'BTC-EUR',
    'XRP': 'XRP-EUR',
    'XLM': 'XLM-EUR', 
    'LTC': 'LTC-EUR',
    'ETH': 'ETH-EUR',
    'ETC': 'ETC-EUR',
    'EOS': 'EOS-EUR',
    'ZRX': 'ZRX-EUR',
    'BCH': 'BCH-EUR'
};

let wallets = {
    batToEur: function (balance) {
        let eur = 0.0;
        let eth = 0.0;
        let price = 0.0;

        eth = cache['BAT-ETH'];
        price = cache['ETH-EUR'];
        if (eth != null && price != null) {
            eur = parseFloat(balance) * parseFloat(price) * parseFloat(eth);
        } else {
            console.log('BAT to Eur not all info available');
            eur = 0.0;
        }
        return eur;
    },
    currencyToEur: function (symbol, balance) {
        let eur = 0.0;
        let price = cache[symbol + '-EUR'];
        if (price != null) {
            eur = parseFloat(balance) * parseFloat(price);
        }
        return eur;
    },
    getBalanceValue: function (req, res) {
        let eur = 0.0;
        authenticatedClient.getAccounts()
            .then(data => {
                let available = 0.0;
                let balance = 0.0;
                let currency = '';
                if ((data != null) && (Symbol.iterator in Object(data))) {
                    for (var item of data) {
                        available = Number(item.available).toFixed(5);
                        balance = Number(item.balance).toFixed(3);
                        currency = item.currency;
                        if (currency == 'BAT') {
                            eur += parseFloat(wallets.batToEur(balance));
                        } else {
                            if (currency == 'EUR') {
                                eur += parseFloat(item.balance);
                            } else {
                                eur += parseFloat(wallets.currencyToEur(currency, balance));
                            }
                        }
                    }
                }
                let balancesCurrencies = {};
                for (var item of data) {
                    let currency = String(item.currency);
                    let pair = eurPair[currency];
                    if (pair != null){
                        currency = pair;
                    } 
                    balancesCurrencies[currency] = item;
                }
                balances['wallets'] = balancesCurrencies;
                balances['balance'] = (eur).toFixed(2);
                return balances;
            })
            .then(data => {
                res.send(data);
            })
            .catch(error => {
                console.log('Error getting accounts');
                res.send(error);
            });

    },
    getBalance: function (req, res) {
        let symbol = req.params.symbol;
        let data = balances.wallets[symbol];
        let result = {};
        if (data !== null) {
            result = data;
        }
        res.send(result);
    },
    findById: function () { },
    add: function () { },
    update: function () { },
    delete: function () { },
}

exports.wallets = wallets;