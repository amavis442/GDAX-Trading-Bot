"use strict";
let cache = require(__dirname + '/../websocket.js').cache;


let products = {
    getTickers: function(req, res) {
        let result = '{"tickers" : [';
        for (var symbol in cache) {
            let item = cache[symbol];
            result += '{"symbol": "'+ symbol + '", "ticker" : "' + item + '"},';
        }
        result = result.slice(0,-1);
        result += ']}';
        res.send(result);
    },
    getTicker: function(req, res) {
        let symbol = String(req.params.symbol);
        let ticker = cache[symbol];
        let result = {};
        if (ticker != null) {
            result[symbol] = ticker;
        } 
        res.send(result);
    }
}

exports.products = products;