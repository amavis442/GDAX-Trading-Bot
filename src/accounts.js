"use strict";

require('dotenv').config();

let coinbase = require('./coinbase.js');
const colors = require('colors');

const getAccountsCallback = (error, response, data) => {
    let available = 0.0;
    let balance = 0.0;
    if (error)
        return console.log(error);

    if ((data != null) && (Symbol.iterator in Object(data))) {
        
        for (var item of data) {
            available = Number(item.available).toFixed(5);
            balance = Number(item.balance).toFixed(3);

            if (item.available > 0) {
                console.log((item.currency + ":: Available: " + available + ' Balance: ' + balance).green.bold);
            }
            //console.log(item.currency + ":: Available: " + available + ' Balance: ' + balance);
        }
    }
}

//Get the balance of the wallets and execute the trading strategy
coinbase.authenticatedClient.getAccounts(getAccountsCallback);
