"use strict";

require('dotenv').config();

const TelegramBot = require('node-telegram-bot-api');
const TELEGRAMTOKEN = process.env.TELEGRAMTOKEN;
const TELEGRAMCHATID = process.env.TELEGRAMCHATID;

const bot = new TelegramBot(TELEGRAMTOKEN, {polling: true});

exports.bot = bot;
exports.chatid = TELEGRAMCHATID;