"use strict";

require('dotenv').config();

let coinbase = require('./coinbase.js');
let telegram = require('./telegram.js');

let CoinbasePro = coinbase.CoinbasePro;
let bot = telegram.bot;

let authenticatedClient = coinbase.authenticatedClient;
let websocket = null;
let filled = null;

const getProductTickerCallback = (error, response, data) => {
    if (error)
        return console.log(error);

    if (data != null) {
        currentPrice = parseFloat(data.bid);
    } else {
        console.log('No price avail');
    }
}

const init_ws_stream = () => {
    websocket = new CoinbasePro.WebsocketClient(['XRP-EUR','XLM-EUR','BTC-EUR','ETC-EUR','ETH-EUR','EOS-EUR','LTC-EUR', 'BAT-ETH'], coinbase.WS_URI, {
        key: coinbase.KEY,
        secret: coinbase.SECRET,
        passphrase: coinbase.PASSPHRASE,
    }, {
            heartbeat: true,
            channels: ['user', 'heartbeat']
        })

    websocket.on('message', (data) => {
        switch (data.type) {
            case "heartbeat":
            case "subscriptions":
                return
            default:
                process_ws_message(data)
                break
        }
    })

    websocket.on('error', (error) => {
        console.log(error)
    })

    websocket.on('close', (data) => {
        ws_reconnect(websocket, data)
    })
}

const ws_reconnect = (ws, data) => {
    console.log(`CoinbasePro websocket disconnected with data: ${data}`)
    // try to re-connect the first time...
    ws.connect()
    let count = 1
    // attempt to re-connect every 30 seconds.
    // TODO: maybe use an exponential backoff instead
    const interval = setInterval(() => {
        if (!ws.socket) {
            console.log(`Reconnecting to CoinbasePro (attempt ${count++})`)
            //count++
            ws.connect()
        } else {
            console.log('CoinbasePro reconnected')
            clearInterval(interval)
        }
    }, 10000)
}

const sendMessage = (error, response, data) => {
    if (error) {
        console.log(error)
        return
    }
    console.log(data)
    filled = data[0]
    bot.sendMessage(telegram.chatid, `${filled.created_at} :: _*${filled.side}* ${filled.product_id}_ *Filled* for ${filled.size} price ${filled.price} euro. fee ${filled.fee}.`,{'parse_mode': 'Markdown'});
}

/**
 *
 */
const process_ws_message = (data) => {
    switch (data.type) {
        case 'done': {
            switch (data.reason) {
                case 'canceled':
                    bot.sendMessage(telegram.chatid, `${data.time} :: _${data.side} ${data.product_id}_ *Canceled* for ${data.remaining_size}.`,{'parse_mode': 'Markdown'});
                    break
                case 'filled':
                    authenticatedClient.getFills({order_id: data.order_id}, sendMessage)
                    break
            }
        } break
        case 'match': {
            let order_id = null
            switch (data.side) {
                case 'buy':
                    order_id = data.maker_order_id
                    break
                case 'sell':
                    order_id = data.taker_order_id
                    break
            }
            if (!order_id) return
        } break
    }
}

function main() {
    console.log('*Klaar om watchen*');
    //bot.sendMessage(telegram.chatid,'*Klaar om watchen*',{'parse_mode': 'Markdown'});
    init_ws_stream();
}
    
main();