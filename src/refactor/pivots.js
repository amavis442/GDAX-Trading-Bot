"use strict";

let stdio = require('stdio');

let ops = stdio.getopt({
    'high': {key: 'h', args: 1, mandatory: true, description: 'High of previous day'},
    'low': {key: 'l',args: 1, mandatory: true, description: 'Low of previous day'},
    'close': {key: 'c', args: 1, mandatory: true, description: 'Close of previous day'}
});

let HIGH = parseFloat(ops.high);
let LOW = parseFloat(ops.low);
let CLOSE = parseFloat(ops.close);

let PP = 0.0;
let R1,R2,R3 = 0.0;
let S1,S2,S3 = 0.0;
let RANGE = HIGH - LOW; 

PP = (HIGH + LOW + CLOSE) / 3;
R1 = (2*PP) - LOW;
R2 = PP + HIGH - LOW;
R3 = HIGH + 2*(PP - LOW);

S1 = (2*PP) - HIGH;
S2 = PP - HIGH + LOW;
S3 = LOW - 2*(HIGH - PP);

console.log('R3: ' + (R3).toFixed(4));
console.log('R2: ' + (R2).toFixed(4));
console.log('R1: ' + (R1).toFixed(4));
console.log('PP: ' + (PP).toFixed(4));
console.log('S1: ' + (S1).toFixed(4));
console.log('S2: ' + (S2).toFixed(4));
console.log('S3: ' + (S3).toFixed(4));