"use strict";

let stdio = require('stdio');

let ops = stdio.getopt({
    'weekly': {key: 'w', args: 1, mandatory: true, description: 'Weekly pivot'},
    'day': {key: 'd',args: 1, mandatory: true, description: 'Daily pivot'},
    'open': {key: 'o', args: 1, mandatory: true, description: 'Open new day'}
});

let WEEKLY = parseFloat(ops.weekly);
let DAY = parseFloat(ops.day);
let OPEN = parseFloat(ops.open);
let SIDE = '';
let ENTRY = 0.0;
let SL = 0.0;
let TP = 0.0;

if (OPEN > WEEKLY) {
    SIDE = 'buy/long';
    ENTRY = DAY + 0.0020;
    SL = ENTRY - 0.0030;
    TP = ENTRY + 0.0040;
}

if (OPEN < WEEKLY) {
    SIDE = 'sell/short';
    ENTRY = DAY - 0.0020;
    SL = ENTRY + 0.0030;
    TP = ENTRY - 0.0040;
}
console.log('Side:        ' + SIDE);
console.log('Entry:       ' + (ENTRY).toFixed(4));
console.log('Stoploss:    ' + (SL).toFixed(4));
console.log('Take Profit: ' + (TP).toFixed(4));
console.log('');
console.log('Note: This strat is only valid for EUR/USD, GBP/USD pair.');
console.log('Close trade after London session has closed even if it is at a loss.');