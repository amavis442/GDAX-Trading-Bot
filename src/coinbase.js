"use strict";

let baseDir = require('path').resolve(__dirname + '/../');
require('dotenv').config({ path: baseDir+ '/.env'});

const CoinbasePro = require('coinbase-pro');

const PASSPHRASE = process.env.TRADING_BOT_PASSPHRASE;
const KEY = process.env.TRADING_BOT_KEY;
const SECRET = process.env.TRADING_BOT_SECRET;
const COINBASEPRO_URI = 'https://api.pro.coinbase.com';
const WS_URI = 'wss://ws-feed.pro.coinbase.com';

let authenticatedClient = new CoinbasePro.AuthenticatedClient(KEY, SECRET, PASSPHRASE, COINBASEPRO_URI);
let publicClient = new CoinbasePro.PublicClient(COINBASEPRO_URI);

exports.authenticatedClient = authenticatedClient;
exports.publicClient = publicClient;
exports.CoinbasePro = CoinbasePro;

exports.COINBASEPRO_URI = COINBASEPRO_URI;
exports.WS_URI = WS_URI;

exports.KEY = KEY;
exports.SECRET = SECRET;
exports.PASSPHRASE = PASSPHRASE;
