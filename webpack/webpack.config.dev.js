'use strict'

const { VueLoaderPlugin } = require('vue-loader')
const path = require('path');

module.exports = {
  mode: 'development',
  entry: [
    './src/frontend/app.js'
  ],
  output: {
    path: __dirname + '/../public/assets/',
    filename: 'app.js'
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
    }
  },
  module: {
    
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      { 
        test: /\.html$/, 
        use: 'vue-template-loader' 
      },
      {
        test: /\.(png|jpg)/,
        loader: 'file-loader',
        options: {
          // ...
        }
      },
      {
        // Loaders that transform css into a format for webpack consumption should be post loaders (enforce: 'post')
        enforce: 'post',
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      }
    ],
  },
  devServer: {
    contentBase:  [
      path.join(__dirname, '/../public'),
    ],
    publicPath: '/assets/',
    compress: true,
    port: 9000
  },
  plugins: [
    new VueLoaderPlugin()
  ]
}