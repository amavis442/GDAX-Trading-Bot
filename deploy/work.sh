#!/bin/bash
set -e

### Configuration ###

APP_DIR=/home/deployer/GDAX-Trading-Bot
GIT_URL=git@gitlab.com:amavis442/GDAX-Trading-Bot.git
RESTART_ARGS=

# Uncomment and modify the following if you installed Passenger from tarball
#export PATH=/path-to-passenger/bin:$PATH


### Automation steps ###

set -x

# Pull latest code
if [[ -e $APP_DIR ]]; then
  cd $APP_DIR
  git pull origin master
else
  git clone $GIT_URL $APP_DIR
  cd $APP_DIR
fi

# Install dependencies
npm install --production
npm prune --production

echo
echo "---- Reload supervisorctl -----"
sudo /usr/bin/supervisorctl reload watcher